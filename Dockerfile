# SPDX-FileCopyrightText: OpenTalk GmbH <mail@opentalk.eu>
# SPDX-FileCopyrightText: Contributors to etherpad-lite Project <https://github.com/ether/etherpad-lite/>
#
# SPDX-License-Identifier: Apache-2.0

FROM node:23-alpine3.21 AS adminbuild
RUN npm install -g pnpm@9.0.4
WORKDIR /opt/etherpad-lite
COPY etherpad-lite .
RUN pnpm install
RUN pnpm run build:ui

FROM node:23-alpine3.21 AS build
LABEL maintainer="OpenTalk, https://opentalk.eu/"

# Set these arguments when building the image from behind a proxy
ARG http_proxy=
ARG https_proxy=
ARG no_proxy=

ARG TIMEZONE=

RUN \
  [ -z "${TIMEZONE}" ] || { \
  apk add --no-cache tzdata && \
  cp /usr/share/zoneinfo/${TIMEZONE} /etc/localtime && \
  echo "${TIMEZONE}" > /etc/timezone; \
  }
ENV TIMEZONE=${TIMEZONE}

ARG SETTINGS=./settings.json.docker

# plugins to install while building the container. By default no plugins are
# installed.
# If given a value, it has to be a space-separated, quoted list of plugin names.
#
# EXAMPLE:
#   ETHERPAD_PLUGINS="ep_codepad ep_author_neat"
ARG ETHERPAD_PLUGINS="ep_auth_session"
ARG ETHERPAD_LOCAL_PLUGINS="/ep_read_session"

# Remove the etherpad welcome text from 
ENV DEFAULT_PAD_TEXT=""
# Hide the integrated etherpad chat
ENV SHOW_CHAT=false
# Don't show errors in the pad text
ENV SUPPRESS_ERRORS_IN_PAD_TEXT=true
# Prevent users from creating pads
ENV EDIT_ONLY=true

# The ep_read_session plugin requires these options.
ENV AUTHENTICATION_METHOD="apikey"
ENV REQUIRE_AUTHENTICATION=true
ENV REQUIRE_AUTHORIZATION=true
ENV REQUIRE_SESSION=true

# Control whether abiword will be installed, enabling exports to DOC/PDF/ODT formats.
# By default, it is not installed.
# If given any value, abiword will be installed.
#
# EXAMPLE:
#   INSTALL_ABIWORD=true
ARG INSTALL_ABIWORD=

# Control whether libreoffice will be installed, enabling exports to DOC/PDF/ODT formats.
# By default, it is not installed.
# If given any value, libreoffice will be installed.
#
# EXAMPLE:
#   INSTALL_LIBREOFFICE=true
ARG INSTALL_SOFFICE=true

# Where to find the soffice binary
ENV SOFFICE=/usr/bin/soffice

# By default, Etherpad container is built and run in "production" mode. This is
# leaner (development dependencies are not installed) and runs faster (among
# other things, assets are minified & compressed).
ENV NODE_ENV=production
ENV ETHERPAD_PRODUCTION=true

# Install dependencies required for modifying access.
RUN apk add --no-cache shadow bash

# Follow the principle of least privilege: run as unprivileged user.
#
# Running as non-root enables running this image in platforms like OpenShift
# that do not allow images running as root.
#
# If any of the following args are set to the empty string, default
# values will be chosen.
ARG EP_HOME=
ARG EP_UID=5001
ARG EP_GID=0
ARG EP_SHELL=

RUN groupadd --system ${EP_GID:+--gid "${EP_GID}" --non-unique} etherpad && \
  useradd --system ${EP_UID:+--uid "${EP_UID}" --non-unique} --gid etherpad \
  ${EP_HOME:+--home-dir "${EP_HOME}"} --create-home \
  ${EP_SHELL:+--shell "${EP_SHELL}"} etherpad

ARG EP_DIR=/opt/etherpad-lite
RUN mkdir -p "${EP_DIR}" && chown etherpad:etherpad "${EP_DIR}"

# the mkdir is needed for configuration of openjdk-11-jre-headless, see
# https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=863199

RUN  \
  mkdir -p /usr/share/man/man1 && \
  npm install pnpm@9.0.4 -g  && \
  apk update && apk upgrade && \
  apk add --no-cache \
  ca-certificates \
  curl \
  git \
  patch \
  ${INSTALL_ABIWORD:+abiword abiword-plugin-command} \
  ${INSTALL_SOFFICE:+libreoffice openjdk8-jre libreoffice-common}

USER etherpad

WORKDIR "${EP_DIR}"

COPY --chown=etherpad:etherpad ./etherpad-lite/${SETTINGS} ./settings.json
COPY --chown=etherpad:etherpad ./etherpad-lite/var ./var
COPY --chown=etherpad:etherpad ./etherpad-lite/bin ./bin
COPY --chown=etherpad:etherpad ./etherpad-lite/pnpm-workspace.yaml ./etherpad-lite/package.json ./

COPY --chown=etherpad:etherpad ./etherpad-lite/src ./src
COPY --chown=etherpad:etherpad --from=adminbuild /opt/etherpad-lite/src/templates/admin ./src/templates/admin
COPY --chown=etherpad:etherpad --from=adminbuild /opt/etherpad-lite/src/static/oidc ./src/static/oidc

# copy custom plugin:
COPY --chown=etherpad:etherpad ./ep_read_session/ /ep_read_session

# apply patches:
COPY --chown=etherpad:etherpad 0001-fix-unknown-session-delete.patch ./
RUN patch -p1 < 0001-fix-unknown-session-delete.patch

RUN bin/installDeps.sh && \
  rm -rf ~/.npm && \
  rm -rf ~/.local && \
  rm -rf ~/.cache && \
  if [ ! -z "${ETHERPAD_PLUGINS}" ] || [ ! -z "${ETHERPAD_LOCAL_PLUGINS}" ] || [ ! -z "${ETHERPAD_GITHUB_PLUGINS}" ]; then \
  pnpm run plugins i ${ETHERPAD_PLUGINS} ${ETHERPAD_LOCAL_PLUGINS:+--path ${ETHERPAD_LOCAL_PLUGINS}} ${ETHERPAD_GITHUB_PLUGINS:+--github ${ETHERPAD_GITHUB_PLUGINS}}; \
  fi

# Copy the configuration file.
COPY --chown=etherpad:etherpad ./etherpad-lite/${SETTINGS} "${EP_DIR}"/settings.json

COPY --chown=etherpad:etherpad ./entrypoint.sh .

USER etherpad

HEALTHCHECK --interval=5s --timeout=3s \
  CMD curl --silent http://localhost:9001/health | grep -E "pass|ok|up" > /dev/null || exit 1

EXPOSE 9001
ENTRYPOINT [ "./entrypoint.sh" ]
